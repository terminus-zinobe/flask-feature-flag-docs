# Flask feature flag

Tool to activate and deactivate project functionalities

## Environment
- Create
    ```shell
    $ python3 -m venv venv
    ```
- Activate
    ```shell
    $ source venv/bin/activate
    ```
- Deactivate
    ```shell
    $ deactivate
    ```

## Package installation
- Installation
    ```shell
    $ pip3 install flask-feature-flag
    ```

- You should add this to your `config.py`
    ```python
    FEATURE_FLAGS = {
        'ROUTE_ENABLED': os.environ.get('ROUTE_ENABLED', True)
    }
    ```
    `FEATURE_FLAGS` is  required

## HTML
- Create html
    ```shell
    $ make html
    ```

- View html
    ./build/html/index.html

## Run in docker

    ```shell
    $ docker-compose up --build
    ```

## Refs:
- [Sphinx](http://www.sphinx-doc.org/en/master/)
- [Theme](https://sphinx-themes.org/html/sphinx-glpi-theme/glpi/index.html)
- [httpdomain](https://sphinxcontrib-httpdomain.readthedocs.io/en/stable/)
