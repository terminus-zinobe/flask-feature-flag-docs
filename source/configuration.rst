Configuration
=============

Setting client environment variables

Config
------

You should add this to your ``config.py``

    - Feature flag type availables.
        * FLASK_CONFIG
        * MONGO

    Example::

        FEATURE_FLAG_TYPE = 'FLASK_CONFIG'


If you use the FLASK_CONFIG type, it is required to add the following:

    Example::

        FEATURE_FLAGS = { # Variable required
            'YOUR_FEATURE_FLAG': os.environ.get('YOUR_FEATURE_FLAG', True)
        }
