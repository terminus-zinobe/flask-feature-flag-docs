User Guide
==========

Here you will learn how to use the package correctly and you will see useful examples

Decorators
----------

List of available decorators::

    is_enabled
    command_enabled
    route_enabled
    use_case_enabled
    on


is_enabled
----------

This decorator allows to activate or deactivate a functionality and receives as parameters a function to return in case feature is disabled and the name of the feature

Example::

    from flask_feature_flag import Flag

    flag = Flag()

    def error():
        return dict(massage='this is a mistake')

    @flag.is_enabled(error, 'YOUR_FEATURE_NAME')
    def hello(name):
        return dict(message=f'Hi, {name}')


command_enabled
---------------

This is a decorator that activates or deactivates a command and receives as parameter the name of the feature

Example::

    import click
    from flask_feature_flag import Flag

    flag = Flag()

    @flag.command_enabled('YOUR_FEATURE_NAME')
    @click.command('hello')
    def hello():
        click.echo('Hello')


route_enabled
-------------

This is a decorator that activates or deactivates a flask route and receives as parameter the name of the feature
    
Example::

    from flask import Flask
    from flask_feature_flag import Flag

    app = Flask(__name__)
    flag = Flag()

    @flag.route_enabled('YOUR_FEATURE_NAME')
    @app.route('/')
    def hello_world():
        return 'Hello, World!'


use_case_enabled
----------------

This is a decorator that activates or deactivates a use case class and receives as parameter the name of the feature


Example::

    from flask_feature_flag import Flag

    flag = Flag()
    
    class SignUpUseCase:

        @flag.use_case_enabled('YOUR_FEATURE_NAME')
        def handle(self):
            return dict(http_code=200, message='OK')


on
--

This is a function that activates or deactivates a feature


Example::

    from flask_feature_flag import Flag

    flag = Flag()
    
    if flag.on('YOUR_FEATURE_NAME'):
        print('HelloWorld')
