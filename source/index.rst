Welcome to Flask-Feature-Flag's documentation!
====================================================

Flask Feature Flag is a tool to activate and deactivate project functionalities

Get the code
------------

The `source <https://gitlab.com/terminus-zinobe/flask-feature-flag>`_ is available on GitLab. 

.. toctree::
    :maxdepth: 2
    :caption: Contents:
    :glob:

    install
    configuration
    userguide



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
